Pod::Spec.new do |spec|

  spec.name                     = "C9OMediaManager"
  spec.version                  = "0.0.3"
  spec.summary                  = "C9OMediaManager framework is provided by Cloud9 Online LLC to handle media releated things for Cloud9 Online applications."
  spec.description              = "C9OMediaManager contains the functionality to take input of meditation/meditation category images names and download them for user. It also provides a view to load those downloaded images."
  spec.homepage                 = "https://gitlab.com/c9opublicframeworks/c9omediamanagerframework"
  spec.license                  = { :type => 'MIT', :text => <<-LICENSE
                                        Copyright © 2021 Cloud9 Online LLC.
                                    LICENSE
                                  }
  spec.author                   = { "Cloud9 Online" => "cloud9online@gmail.com" }
  spec.ios.deployment_target    = "11.0"
  spec.source                   = { :git => "https://gitlab.com/c9opublicframeworks/c9omediamanagerframework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks      = "C9OMediaManager.framework"
  spec.swift_versions           = ['5.0']
    
  spec.dependency                 'C9OCore'
  spec.dependency                 'Amplify'
  spec.dependency                 'AmplifyPlugins/AWSS3StoragePlugin'
  spec.dependency                 'AmplifyPlugins/AWSCognitoAuthPlugin'
  spec.weak_frameworks          = 'Foundation', 'SystemConfiguration'

  spec.user_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

end
