# C9OMediaManager

C9OMediaManager framework is provided by Cloud9 Online LLC to handle media releated things for Cloud9 Online applications.

## Requirements
- Xcode 12.0 or later
- iOS 11.0 or later
- Swift 5.0 or later

## Installation
### Cocoapods
1. This framework is available through [CocoaPods](http://cocoapods.org). 
2. Include C9OCore in your `Pod File`:

```
pod 'C9OMediaManager'
```
   
## Usage
### C9OImageContainer
C9OMediaManager provides a helper View to show any meditation's and meditation category's images. To get any image file for a meditation:

1. Import following in your desired swift file.

```swift
import C9OCore
import C9OMediaManager
```

2. Open your Storyboard or .XIB file, select your superview, drag a UIView and set it's Custom Class name to **C9OImageContainer**.
3. Create and attach an outlet for our **C9OImageContainer** view.

```swift
@IBOutlet weak var imageContainerView: C9OImageContainer!
```

4. Use **setImage** method of **C9OImageContainer** class to set details of image to be downloaded.

```swift
imageContainerView.setImage(fileName: String, type: C9ODownloadType, overLay: Bool, overLayWithOpacity: CGFloat)
```

- `fileName` Name of file to be downloaded. In C9OCore, we've two classes **Meditation** & **MeditationCategory**. For Meditation, you can use **coverImageFileName** and pass its value here. For MeditationCategory, you can use **categoryName** and pass its value here.
- `type` Enum provided by **C9OCore** framework which have following options:
    - `coverThumbnail`
    - `cover`
    - `meditationCategory`
    - `challengeImage`
    - `themeImage`
    - `howToVideo`
    - `guideImage`
- `overLay` To add black overLay above the image. By default, its value is false.
- `overLayWithOpacity` To set the opacity of black overLay. Bye default, its value is 0.4.

### Download Image/Audio
C9OMediaManager provides a method for downloading media i.e. image, audio. To download any media:

1. Import following in your desired swift file.

```swift
import C9OCore
import C9OMediaManager
```

2. Send request for download using following method:
```swift
C9OMediaManager.shared.addDownloadOperation(downloadType: C9ODownloadType, downloadFromFileName: String) { (error) in
}
```

## Observers
### Subscribe
```swift
let observerClient = C9OMediaManager.shared.addObserver(observer: self)
```

After subscribing for observer, you can listen for following delegate functions:
```swift
extension ViewController: C9OMediaManagerObserver {
        
    func didUpdateDownload(downloadType: C9ODownloadType, downloadFromFileName: String, progress: Float) {
    }

}
```

### Unsubscribe
```swift
C9OMediaManager.shared.removeObserver(client: observerClient)
```
